<?php
	/*
	 * GLO: Login Page
	 * Path: /login
	 */
	$app->group('/login', function () use ($app, $container) {
		$app->get('[/{action}]', function ($request, $response, $args) use ($app) {
			$action = (isset($args["action"])) ? $args["action"] : null;
			$csrf_name_key = $this->csrf->getTokenNameKey();
			$csrf_value_key = $this->csrf->getTokenValueKey();
			$csrf_name = $request->getAttribute('csrf_name');
			$csrf_value = $request->getAttribute('csrf_value');
			if (isset($_SESSION["user"])){
				$breadcrumb[_("menu.dashboard")] = "/home";
				$primary_title = _("menu.dashboard");
				header ("Location: ".$_ENV["hostname"]."/home");
				exit();
			}else{
				require $_ENV["GLO_ADMIN_PAGES"].'/User/login.php';
			}
			return $response;
		})->add($container->get('csrf'));
	});

	/*
	 * GLO: Home Page
	 * Path: /home
	 */
	$app->group('/home', function () use ($app, $container) {
		$app->get('[/{action}]', function ($request, $response, $args) use ($app) {
			$csrf_name_key = $this->csrf->getTokenNameKey();
			$csrf_value_key = $this->csrf->getTokenValueKey();
			$csrf_name = $request->getAttribute('csrf_name');
			$csrf_value = $request->getAttribute('csrf_value');
			$action = (isset($args["action"])) ? $args["action"] : null;
			if (isset($_SESSION["user"])){
				$breadcrumb[_("menu.overview")] = "/home";
				$primary_title = _("menu.dashboard");
				switch ($_SESSION["user"]->getRole()->getId()){
					case Role::ADMINISTRATOR:
						require $_ENV["GLO_ADMIN_PAGES"].'/User/home.php';
					break;
					default:
						header ("Location: ".$_ENV["hostname"]."/403");
					break;
				}
			}else{
				header ("Location: ".$_ENV["hostname"]."/login");
			}
			return $response;
		})->add($container->get('csrf'));
	});


	/*
	 * GLO: Reset Password
	 * Path: /reset[/{token}]
	 */
	$app->group('/reset', function () use ($app, $container) {
		$app->get('[/{token}]', function ($request, $response, $args) use ($app) {
			$token = (isset($args["token"])) ? $args["token"] : null;
			if (isset($_SESSION["user"])){
				header ("Location: ".$_ENV["hostname"]."/home");
			}else{
				if ($token!=null){
					$valid_token = User::validateResetToken($token);
				}
				$csrf_name_key = $this->csrf->getTokenNameKey();
				$csrf_value_key = $this->csrf->getTokenValueKey();
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				require $_ENV["GLO_ADMIN_PAGES"].'/User/reset.php';
			}
			return $response;
		})->add($container->get('csrf'));
	});

	$app->get('/confirm/{token}', function ($request, $respons, $args) use($app) {
		$token = (isset($args["token"])) ? $args["token"] : null;
		$csrf_name_key = $this->csrf->getTokenNameKey();
		$csrf_value_key = $this->csrf->getTokenValueKey();
		$csrf_name = $request->getAttribute('csrf_name');
		$csrf_value = $request->getAttribute('csrf_value');
 		if (User::confirmEmail($token)){
 			if (isset($_SESSION["user"])){
 				header ("Location: ".$_ENV["hostname"]."/home/confirmed");
 			}else{
 				header ("Location: ".$_ENV["hostname"]."/login/confirmed");
 			}
 		}else{
 			if (isset($_SESSION["user"])){
 				header ("Location: ".$_ENV["hostname"]."/home");
 			}else{
 				header ("Location: ".$_ENV["hostname"]."/login");
 			}
 		}
	})->add($container->get('csrf'));

	/*
	 * E-mails
	 */
	$app->group('/emails', function () use ($app, $container){
		$breadcrumb[_("menu.emails")] = "/emails";
		$app->get('', function ($request, $response, $args) use ($app, $container, $breadcrumb){
			$csrf_name_key = $this->csrf->getTokenNameKey();
			$csrf_value_key = $this->csrf->getTokenValueKey();
			$csrf_name = $request->getAttribute('csrf_name');
			$csrf_value = $request->getAttribute('csrf_value');
			if (isset($_SESSION["user"])){
				if ($_SESSION["user"]->isAllowed(Permission::EMAIL_MANAGEMENT_READ)){
					$primary_title = _("menu.emails");
					require $_ENV["GLO_ADMIN_PAGES"].'/Email/email.dashboard.php';
				}else{
					header("Location: ".$_ENV["hostname"]."/403");
				}
			}else{
				header("Location: ".$_ENV["hostname"]."/login");
			}
			return $response;
		})->add($container->get('csrf'));

		$app->group('/templates', function () use ($app, $container, $breadcrumb){
			$app->get('/new', function ($request, $response, $args) use ($app, $container, $breadcrumb){
				$csrf_name_key = $this->csrf->getTokenNameKey();
				$csrf_value_key = $this->csrf->getTokenValueKey();
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				if (isset($_SESSION["user"])){
					if ($_SESSION["user"]->isAllowed(Permission::EMAIL_MANAGEMENT_CREATE)){
						$breadcrumb[_("menu.email.new")] = "/emails/templates/new";
						$primary_title = _("email.new.template");
						require $_ENV["GLO_ADMIN_PAGES"].'/Email/Template/template.new.php';
					}else{
						header("Location: ".$_ENV["hostname"]."/403");
					}
				}else{
					header("Location: ".$_ENV["hostname"]."/login");
				}
				return $response;
			})->add($container->get('csrf'));

			$app->get('/edit/{id}', function ($request, $response, $args) use ($app, $container, $breadcrumb){
				$csrf_name_key = $this->csrf->getTokenNameKey();
				$csrf_value_key = $this->csrf->getTokenValueKey();
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				if (isset($_SESSION["user"])){
					if ($_SESSION["user"]->isAllowed(Permission::EMAIL_MANAGEMENT_UPDATE)){
						$id = (isset($args["id"])) ? intval($args["id"]) : null;
						if ($id!=null && $id>0){
							$email_template = new EmailTemplate($id);
							$primary_title = $email_template->getTitle();
							$breadcrumb[_("menu.email.edit")] = "/emails/templates/edit/".$email_template->getId();
							require $_ENV["GLO_ADMIN_PAGES"].'/Email/Template/template.edit.php';
						}else{
							header("Location: ".$_ENV["hostname"]."/emails/templates");
						}
					}else{
						header("Location: ".$_ENV["hostname"]."/403");
					}
				}else{
					header("Location: ".$_ENV["hostname"]."/login");
				}
				return $response;
			})->add($container->get('csrf'));

			$app->get('/view/{id}', function ($request, $response, $args) use ($app, $container, $breadcrumb){
				$csrf_name_key = $this->csrf->getTokenNameKey();
				$csrf_value_key = $this->csrf->getTokenValueKey();
				$csrf_name = $request->getAttribute('csrf_name');
				$csrf_value = $request->getAttribute('csrf_value');
				if (isset($_SESSION["user"])){
					if ($_SESSION["user"]->isAllowed(Permission::EMAIL_MANAGEMENT_READ)){
						$id = (isset($args["id"])) ? intval($args["id"]) : null;
						if ($id!=null && $id>0){
							$email_template = new EmailTemplate($id);
							$primary_title = $email_template->getTitle();
							$breadcrumb[_("menu.email.view")] = "/emails/templates/view/".$email_template->getId();
							require $_ENV["GLO_ADMIN_PAGES"].'/Email/Template/template.view.php';
						}else{
							header("Location: ".$_ENV["hostname"]."/emails/templates");
						}
					}else{
						header("Location: ".$_ENV["hostname"]."/403");
					}
				}else{
					header("Location: ".$_ENV["hostname"]."/login");
				}
				return $response;
			})->add($container->get('csrf'));
		});
	});

	/*
	 * User
	 */
	$app->group('/user', function () use ($app, $container){
		$app->get('/notifications', function ($request, $response, $args) use ($app, $container){
			$csrf_name_key = $this->csrf->getTokenNameKey();
			$csrf_value_key = $this->csrf->getTokenValueKey();
			$csrf_name = $request->getAttribute('csrf_name');
			$csrf_value = $request->getAttribute('csrf_value');
			if (isset($_SESSION["user"])){
				$user_notifications = $_SESSION["user"]->getAllNotifications();
				require $_ENV["GLO_ADMIN_PAGES"].'/user_notifications.php';
			}else{
				header("Location: ".$_ENV["hostname"]."/login");
			}
			return $response;
		})->add($container->get('csrf'));
	});


	/*
	 * Users
	 */
	$app->group('/users', function () use ($app, $container){
		unset($app->breadcrumb);
		$breadcrumb[_("menu.users")] = '/users';
		$app->get('', function ($request, $response, $args) use ($app, $container, $breadcrumb){
			$csrf_name_key = $this->csrf->getTokenNameKey();
			$csrf_value_key = $this->csrf->getTokenValueKey();
			$csrf_name = $request->getAttribute('csrf_name');
			$csrf_value = $request->getAttribute('csrf_value');
			if (isset($_SESSION["user"])){
				if ($_SESSION["user"]->isAllowed(Permission::USER_MANAGEMENT_READ)){
					$primary_title = _("user.dashboard");
					$users = User::find();
					require $_ENV["GLO_ADMIN_PAGES"].'/User/user.dashboard.php';
				}else{
					header("Location: ".$_ENV["hostname"]."/403");
				}
			}else{
				header("Location: ".$_ENV["hostname"]."/login");
			}
			return $response;
		})->add($container->get('csrf'));

		$app->get('/new', function ($request, $response, $args) use ($app, $container, $breadcrumb){
			$breadcrumb[_("menu.new")] = "/users/new";
			$csrf_name_key = $this->csrf->getTokenNameKey();
			$csrf_value_key = $this->csrf->getTokenValueKey();
			$csrf_name = $request->getAttribute('csrf_name');
			$csrf_value = $request->getAttribute('csrf_value');
			if (isset($_SESSION["user"])) {
				if ($_SESSION["user"]->isAllowed(Permission::USER_MANAGEMENT_CREATE)){
					$primary_title = _('user.new');
					require $_ENV["GLO_ADMIN_PAGES"].'/User/user.new.php';
				}else{
					header("Location: ".$_ENV["hostname"]."/403");
				}
			}else{
				header("Location: ".$_ENV["hostname"]."/login");
			}
			return $response;
		})->add($container->get('csrf'));

		$app->get('/view/{id}', function ($request, $response, $args) use ($app, $container, $breadcrumb){
			$csrf_name_key = $this->csrf->getTokenNameKey();
			$csrf_value_key = $this->csrf->getTokenValueKey();
			$csrf_name = $request->getAttribute('csrf_name');
			$csrf_value = $request->getAttribute('csrf_value');
			if (isset($_SESSION["user"])){
				$id = (isset($args["id"])) ? intval($args["id"]) : null;
				if ($id!=null && $id>0){
					if ($_SESSION["user"]->isAllowed(Permission::USER_MANAGEMENT_READ) ||
						($_SESSION["user"]->isAllowed(Permission::USER_READ) && $_SESSION["user"]->getId() == $id)){
						$user = new User($id);
						if ($user->getId()!=null){
							$breadcrumb[_("menu.view")] = "/users/view/".$user->getId();
							$primary_title = $user->getName();
							require $_ENV["GLO_ADMIN_PAGES"].'/User/user.view.php';
						}else{
							header("Location: ".$_ENV["hostname"]."/users");
						}
					}else{
						header("Location: ".$_ENV["hostname"]."/403");
					}
				}else{
					header("Location: ".$_ENV["hostname"]."/users");
				}
			}else{
				header("Location: ".$_ENV["hostname"]."/login");
			}
			return $response;
		})->add($container->get('csrf'));

		$app->get('/edit/{id}', function ($request, $response, $args) use ($app, $container, $breadcrumb){
			$csrf_name_key = $this->csrf->getTokenNameKey();
			$csrf_value_key = $this->csrf->getTokenValueKey();
			$csrf_name = $request->getAttribute('csrf_name');
			$csrf_value = $request->getAttribute('csrf_value');
			if (isset($_SESSION["user"])){
				$id = (isset($args["id"])) ? intval($args["id"]) : null;
				if ($id!=null && $id>0){
					if ($_SESSION["user"]->isAllowed(Permission::USER_MANAGEMENT_UPDATE) ||
						($_SESSION["user"]->isAllowed(Permission::USER_UPDATE) && $_SESSION["user"]->getId() == $id)){
						$user = new User($id);
						if ($user->getId()!=null){
							$breadcrumb[_("menu.edit")] = "/users/edit/".$user->getId();
							$primary_title = $user->getName();
							require $_ENV["GLO_ADMIN_PAGES"].'/User/user.edit.php';
						}else{
							header("Location: ".$_ENV["hostname"]."/users");
						}
					}else{
						header("Location: ".$_ENV["hostname"]."/403");
					}
				}else{
					header("Location: ".$_ENV["hostname"]."/users");
				}
			}else{
				header("Location: ".$_ENV["hostname"]."/login");
			}
			return $response;
		})->add($container->get('csrf'));
	});
?>
