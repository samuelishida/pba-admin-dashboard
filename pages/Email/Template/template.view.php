<?php require $_ENV["GLO_ADMIN_PAGES"]."/header.php"; ?>
	<!-- Start Page Content -->
	<div class="row">
			<!-- Column -->
			<div class="col-lg-12">
					<div class="card">
							<div class="card-body">
									<div class="card-two">
										<div class="text-right">
											<button value="/emails/templates/edit/<?= $email_template->getId(); ?>" class="btn btn-dark btn-outline m-b-10 link"><i class="fas fa-pencil-alt"></i></button>
										</div>
										<p class="text-center avatar"><i class="far fa-envelope"></i></p>
										<h3 class="no-margin-top"><?= $email_template->getTitle(); ?></h3>
										<div class="desc">
												<?= $email_template->getDescription(); ?>
										</div>
										<div class="clear"></div>
									</div>
									<hr>
									<div class="card-two">
										<?php
											if ($email_template->getMarkdown()){
												$parse_down = new Parsedown();
												print $parse_down->text($email_template->getHtmlBody());
											}else{
												print $email_template->getHtmlBody();
											}
										?>
									</div>
							</div>
					</div>
			</div>
			<!-- Column -->
			<!-- Column -->
			<div class="col-lg-12">
					<div class="card">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs profile-tab" role="tablist">
									<li class="nav-item active"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
									<!--second tab-->
									<div class="tab-pane active" id="settings" role="tabpanel">
											<div class="card-body">
													<div class="row">
														<div class="col-md-4 col-xs-6 b-r"> <strong><?= _("email.title") ?></strong>
																<br>
																<p class="text-muted"><?= $email_template->getTitle(); ?></p>
														</div>
														<div class="col-md-8 col-xs-6 b-r">	<strong><?= _("email.description") ?></strong>
															<br />
															<p class="text-muted">
																<?= $email_template->getDescription() ?>
															</p>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12 b-r"> <strong><?= _("email.subject") ?></strong>
																<br>
																<p class="text-muted"><?= $email_template->getSubject(); ?></p>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12 b-r"> <strong><?= _("email.text_body") ?></strong>
																<br>
																<p class="text-muted"><?php print nl2br($email_template->getTextBody()); ?></p>
														</div>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
			<!-- Column -->
	</div>
	<!-- End Page Content -->
<?php require $_ENV["GLO_ADMIN_PAGES"]."/footer.php"; ?>
