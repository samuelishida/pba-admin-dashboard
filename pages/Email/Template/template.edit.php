<?php require $_ENV["GLO_ADMIN_PAGES"]."/header.php"; ?>
	<!-- Start Page Content -->
	<div class="row">
		<!-- Column -->
		<div class="col-lg-12">
				<div class="card">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs profile-tab" role="tablist">
								<li class="nav-item active"> <a class="nav-link active show" data-toggle="tab" href="#settings" role="tab"><?= _("email.settings") ?></a> </li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
								<!--second tab-->
								<div class="tab-pane active" id="settings" role="tabpanel">
										<div class="card-body">
												<form id="email_template" name="email_template"	action="/api/v1/email/template/edit" method="post" class="validate form-horizontal form-material">
														<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
														<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
														<input type="hidden" name="id" value="<?= $email_template->getId() ?>" />
														<div class="form-group">
																<label class="col-md-12"><?= _("email.template_title") ?></label>
																<div class="col-md-12">
																	<input id="email_template_title" type="text" name="title" placeholder="<?= _("email.template.your_title") ?>" value="<?= $email_template->getTitle() ?>" class="form-control form-control-line" required>
																</div>
														</div>
														<div class="form-group">
																<label class="col-md-12"><?= _("email.description") ?></label>
																<div class="col-md-12">
																	<textarea id="email_template_description" name="description" placeholder="<?= _("email.description.placeholder") ?>" rows="10" class="form-control textarea_editor" style="height: 10em;" required><?= $email_template->getDescription() ?></textarea>
																</div>
														</div>
														<div class="form-group">
																<label class="col-md-12"><?= _("email.subject") ?></label>
																<div class="col-md-12">
																	<input id="email_template_subject" type="text" name="subject" placeholder="<?= _("email.subject") ?>" class="form-control form-control-line" value="<?= $email_template->getSubject() ?>" required />
																</div>
														</div>
														<div class="form-group">
															<label class="col-md-12"><?= _("email.html_body") ?></label>
															<div class="col-md-12">
																<textarea id="email_template_html_body_<?= $email_template->getId(); ?>" name="html_body" placeholder="<?= _("email.template.text_body") ?>" style="height:20em;" rows="20" class="editor form-control form-control-line" required ><?= $email_template->getHtmlBody() ?></textarea>
															</div>
														</div>
														<div class="form-group">
															<div class="col-md-12 text-right">
																<div class="pretty p-switch p-fill">
																	<input id="email_template_markdown" name="markdown" type="checkbox" <?php if ($email_template->getMarkdown()) print 'checked="checked"'; ?> />
																	<div class="state p-success">
																		<label><?= _("email.markdown") ?></label>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group">
																<label class="col-md-12"><?= _("email.text_body") ?></label>
																<div class="col-md-12">
																	<textarea id="email_template_text_body" name="text_body" placeholder="<?= _("email.template.text_body") ?>" style="height:20em;" rows="20" class="form-control textarea_editor" style="height: 10em;" required><?= $email_template->getTextBody() ?></textarea>
																</div>
														</div>
														<div class="form-group">
																<div class="col-sm-12 text-right">
																		<input type="button" name="cancel" value="<?= _("system.cancel") ?>" class="btn btn-danger" onclick="window.history.back();"/>
																		<input type="submit" name="create_email_template" value="<?= _("email.update") ?>" class="btn btn-success" />
																</div>
														</div>
												</form>
										</div>
								</div>
						</div>
				</div>
		</div>
		<!-- Column -->
</div>
	<!-- End Page Content -->
<?php require $_ENV["GLO_ADMIN_PAGES"]."/footer.php"; ?>
<script>
$(document).ready(function(){
	$("#email_template").validate({
		ignore: [],
		errorClass: "invalid-feedback animated fadeInDown",
		errorElement: "div",
		errorPlacement: function(e, a) {
			jQuery(a).parents(".form-group > div").append(e)
		},
		highlight: function(e) {
			jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
		},
		success: function(e) {
			jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
		},
		submitHandler: function(form) {
			$(form).submitHandler();
			return false;
		},
		rules: {
			title: {
				required: true,
				minlength: 1,
				maxlength: 255
			},
			description: {
				required: true,
				minlength: 1,
				maxlength: 1000
			},
			subject: {
				required: true,
				minlength: 1,
				maxlength: 255
			},
			html_body: {
				required: true,
				minlength: 1,
				maxlength: 100000
			},
			text_body: {
				required: true,
				minlength: 1,
				maxlength: 100000
			}
		},
		messages: {
			title: {
				required: <?= _("messages.required") ?>,
				minlength: <?= _("messages.minlength_1") ?>,
				maxlength: <?= _("messages.maxlength_255") ?>
			},
			description: {
				required: <?= _("messages.required") ?>,
				minlength: <?= _("messages.minlength_1") ?>,
				maxlength: <?= _("messages.maxlength_10000") ?>
			},
			subject: {
				required: <?= _("messages.required") ?>,
				minlength: <?= _("messages.minlength_1") ?>,
				maxlength: <?= _("messages.maxlength_255") ?>
			},
			html_body: {
				required: <?= _("messages.required") ?>,
				minlength: <?= _("messages.minlength_1") ?>,
				maxlength: <?= _("messages.maxlength_10000") ?>
			},
			text_body: {
				required: <?= _("messages.required") ?>,
				minlength: <?= _("messages.minlength_1") ?>,
				maxlength: <?= _("messages.maxlength_10000") ?>
			}
		}
	});
});
$('body').on("treat", "#email_template", function(event, response){
	switch (response.status){
		case 200:
			/* HTTP 200: OK */
			if (response.data.success == true){
				$("#email_template_html_body_<?= $email_template->getId(); ?>").data("simplemde").value("");
				$("#email_template_html_body_<?= $email_template->getId(); ?>").data("simplemde").clearAutosavedValue();
				window.location.href = "/emails/templates/view/" + response.data.data.id;
			}else{
				toastr.error('<?= _("system.try.again") ?>','<?= _("system.post.failed") ?>',{
					"positionClass": "toast-top-full-width",
					timeOut: 5000,
					"closeButton": true,
					"debug": false,
					"newestOnTop": true,
					"progressBar": true,
					"preventDuplicates": true,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut",
					"tapToDismiss": false
				})
			}
		break;
		case 400:
			/* HTTP 400: Bad request */
			toastr.error('<?= _("form.field.invalid"); ?>','<?= _("system.post.failed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 403:
			/* HTTP 403: Forbidden */
			toastr.error('<?= _("system.action.not_allowed"); ?>','<?= _("system.post.failed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 404:
			/* HTTP 404: Not found */
			toastr.error('<?= _("system.action.inexistent") ?>','<?= _("system.post.failed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 500:
			/* HTTP 500: Internal server error */
			toastr.error('<?= _("system.server.unavailable"); ?>','<?= _("system.post.failed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		default:
			/* Something is wrong */
			toastr.error('<?= _("system.unknown.error") ?>','<?= _("system.post.failed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
	}
});
</script>
