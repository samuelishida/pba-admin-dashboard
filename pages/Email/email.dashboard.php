<?php require $_ENV["GLO_ADMIN_PAGES"]."/header.php"; ?>
	<!-- Start Page Content -->
	<div class="row">
		<div class="col-lg-12">
				<div class="card">
						<div class="card-body">
								<div class="row">
									<div class="col-lg-8">
										<h4 class="card-title"><?= _("email.template") ?></h4>
										<h6 class="card-subtitle"><?= _("email.template_all") ?></h6>
									</div>
								</div>
								<div class="table-responsive m-t-40">
									<input id="system_email_table_csrf_name" type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
  								<input id="system_email_table_csrf_key" type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
									<table id="system_email_templates_table" style="width: 100%;" class="datatable table table-bordered table-striped">
										<thead>
											<tr>
												<th><?= _("table.title") ?></th>
												<th><?= _("table.description") ?></th>
												<th><?= _("table.date") ?></th>
												<th></th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
						</div>
				</div>
		</div>
	</div>
<!-- End Page Content -->
<?php require $_ENV["GLO_ADMIN_PAGES"]."/footer.php"; ?>
<script>
	$("#system_email_templates_table").DataTable({
		"language": {
			"processing": '<i class="fa fa-spinner fa-pulse"></i> Processing...'
		},
		"dom": '<"top"lf>rt<"bottom"pi><"clear">',
		"columnDefs": [{ "searchable": false, "orderable": false, "targets": 3 },
				{ "responsivePriority": 1, "targets": 0},
				{ "responsivePriority": 2, "targets": 2},
				{ "responsivePriority": 3, "targets": 3},
				{ "responsivePriority": 4, "targets": 1}
			],
		"lengthChange": true,
		"processing": true,
		"serverSide": true,
		"responsive": true,
		"ajax": {
			"url": "/api/v1/email/template/datatable",
			"contentType": "application/json",
			"type": "POST",
			"data": function (d) {
				return JSON.stringify($.extend( {}, d, {"csrf_name" : $("#system_email_table_csrf_name").val(),"csrf_value" : $("#system_email_table_csrf_key").val()}));
			},
			"dataFilter": function (d){
				var response = JSON.parse(d);
				$(this).updateCSRF(response.csrf);
				return JSON.stringify(response.data);
			},
			"error": function (d){
				$("#message").html('<div class="error"><p><i class="fa fa-times-circle"></i> Something went wrong while retrieving table data. Try again later.</p></div>');
				$(".dataTables_processing").addClass("error-message");
				$(".dataTables_processing").html('<i class="fa fa-times"></i> Error');
			}
		}
	});
</script>
