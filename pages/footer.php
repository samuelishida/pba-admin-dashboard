					 </div>
						<!-- End Container fluid	-->
						<!-- footer -->
						<footer class="footer"> © <?= $_ENV["GLO_APP_OWNER"]; ?> - <?= date("Y"); ?> <?= _("system.rights"); ?>.</footer>
						<!-- End footer -->
				</div>
				<!-- End Page wrapper	-->
		</div>
		<!-- End Wrapper -->
	 	<?php require $_ENV["GLO_ADMIN_PAGES"]."/script.php"; ?>

		<script>
			$('body').on("click", '.user-notification', function(e){
				$("#user_notification").val($(this).find("input").val());
				$("#read_notification").submitHandler();
			});
			$('body').on("treat", "#read_notification", function(event, response){
				switch (response.status){
					case 200:
						/* HTTP 200: OK */
						if (response.data.success == true){
							window.location.href = response.data.data.links.view;
						}else{
							toastr.error('<?= _("system.try.again") ?>','<?= _("notification.open.failed") ?>',{
								"positionClass": "toast-top-full-width",
								timeOut: 5000,
								"closeButton": true,
								"debug": false,
								"newestOnTop": true,
								"progressBar": true,
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"tapToDismiss": false
							})
						}
					break;
					case 400:
						/* HTTP 400: Bad request */
						toastr.error('<?= _("form.field.invalid"); ?>','<?= _("notification.open.failed") ?>',{
							"positionClass": "toast-top-full-width",
							timeOut: 5000,
							"closeButton": true,
							"debug": false,
							"newestOnTop": true,
							"progressBar": true,
							"preventDuplicates": true,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"tapToDismiss": false
						})
					break;
					case 403:
						/* HTTP 403: Forbidden */
						toastr.error('<?= _("system.action.not_allowed"); ?>','<?= _("notification.open.failed") ?>',{
							"positionClass": "toast-top-full-width",
							timeOut: 5000,
							"closeButton": true,
							"debug": false,
							"newestOnTop": true,
							"progressBar": true,
							"preventDuplicates": true,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"tapToDismiss": false
						})
					break;
					case 404:
						/* HTTP 404: Not found */
						toastr.error('<?= _("system.action.inexistent") ?>','<?= _("notification.open.failed") ?>',{
							"positionClass": "toast-top-full-width",
							timeOut: 5000,
							"closeButton": true,
							"debug": false,
							"newestOnTop": true,
							"progressBar": true,
							"preventDuplicates": true,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"tapToDismiss": false
						})
					break;
					case 500:
						/* HTTP 500: Internal server error */
						toastr.error('<?= _("system.server.unavailable"); ?>','<?= _("notification.open.failed") ?>',{
							"positionClass": "toast-top-full-width",
							timeOut: 5000,
							"closeButton": true,
							"debug": false,
							"newestOnTop": true,
							"progressBar": true,
							"preventDuplicates": true,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"tapToDismiss": false
						})
					break;
					default:
						/* Something is wrong */
						toastr.error('<?= _("system.unknown.error") ?>', '<?= _("notification.open.failed") ?>',{
							"positionClass": "toast-top-full-width",
							timeOut: 5000,
							"closeButton": true,
							"debug": false,
							"newestOnTop": true,
							"progressBar": true,
							"preventDuplicates": true,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"tapToDismiss": false
						})
					break;
				}
			});
			$('body').on("treat", "#report_bug", function(event, response){
				switch (response.status){
					case 200:
						/* HTTP 200: OK */
						if (response.data.success == true){
							$(".show").removeClass("show");
							toastr.success('<?= _("bug.success.message") ?>','<?= _("bug.success") ?>',{
								"positionClass": "toast-top-full-width",
								timeOut: 5000,
								"closeButton": true,
								"debug": false,
								"newestOnTop": true,
								"progressBar": true,
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"tapToDismiss": false
							})
						}else{
							toastr.error('<?= _("system.try.again") ?>','<?= _("bug.failed") ?>',{
								"positionClass": "toast-top-full-width",
								timeOut: 5000,
								"closeButton": true,
								"debug": false,
								"newestOnTop": true,
								"progressBar": true,
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"tapToDismiss": false
							})
						}
					break;
					default:
						/* Something is wrong */
						toastr.error('<?= _("system.unknown.error") ?>', '<?= _("bug.failed") ?>',{
							"positionClass": "toast-top-full-width",
							timeOut: 5000,
							"closeButton": true,
							"debug": false,
							"newestOnTop": true,
							"progressBar": true,
							"preventDuplicates": true,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"tapToDismiss": false
						})
					break;
				}
			});
			$('body').on("treat", "#ask_feature", function(event, response){
				switch (response.status){
					case 200:
						/* HTTP 200: OK */
						if (response.data.success == true){
							$(".show").removeClass("show");
							toastr.success('<?= _("feature.success.message") ?>','<?= _("feature.success") ?>',{
								"positionClass": "toast-top-full-width",
								timeOut: 5000,
								"closeButton": true,
								"debug": false,
								"newestOnTop": true,
								"progressBar": true,
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"tapToDismiss": false
							})
						}else{
							toastr.error('<?= _("system.try.again") ?>','<?= _("feature.failed") ?>',{
								"positionClass": "toast-top-full-width",
								timeOut: 5000,
								"closeButton": true,
								"debug": false,
								"newestOnTop": true,
								"progressBar": true,
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"tapToDismiss": false
							})
						}
					break;
					default:
						/* Something is wrong */
						toastr.error('<?= _("system.unknown.error") ?>', '<?= _("feature.failed") ?>',{
							"positionClass": "toast-top-full-width",
							timeOut: 5000,
							"closeButton": true,
							"debug": false,
							"newestOnTop": true,
							"progressBar": true,
							"preventDuplicates": true,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"tapToDismiss": false
						})
					break;
				}
			});
			$(document).ready(function(){
				$("#report_bug").validate({
					ignore: [],
					errorClass: "invalid-feedback animated fadeInDown",
					errorElement: "div",
					errorPlacement: function(e, a) {
						jQuery(a).parents(".form-group > div").append(e)
					},
					highlight: function(e) {
						jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
					},
					success: function(e) {
						jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
					},
					submitHandler: function(form) {
						$(form).submitHandler();
						return false;
					},
					rules: {
						title: {
							required: true,
							minlength: 1,
							maxlength: 255
						},
						description: {
							required: true,
							minlength: 1,
							maxlength: 10000
						}
					},
					messages: {
						title: {
							required: "<?= _("form.field.required"); ?>",
							minlength: "<?= sprintf(_("form.field.least.character"), 1); ?>",
							maxlength: "<?= sprintf(_("form.field.max"), 255); ?>"
						},
						description: {
							required: "<?= _("form.field.required"); ?>",
							minlength: "<?= sprintf(_("form.field.least.character"), 1); ?>",
							maxlength: "<?= sprintf(_("form.field.max"), 10000); ?>"
						}
					}
				});
				$("#ask_feature").validate({
					ignore: [],
					errorClass: "invalid-feedback animated fadeInDown",
					errorElement: "div",
					errorPlacement: function(e, a) {
						jQuery(a).parents(".form-group > div").append(e)
					},
					highlight: function(e) {
						jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
					},
					success: function(e) {
						jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
					},
					submitHandler: function(form) {
						$(form).submitHandler();
						return false;
					},
					rules: {
						title: {
							required: true,
							minlength: 1,
							maxlength: 255
						},
						description: {
							required: true,
							minlength: 1,
							maxlength: 10000
						}
					},
					messages: {
						title: {
							required: "<?= _("form.field.required"); ?>",
							minlength: "<?= sprintf(_("form.field.least"), 1); ?>",
							maxlength: "<?= sprintf(_("form.field.max"), 255); ?>"
						},
						description: {
							required: "<?= _("form.field.required"); ?>",
							minlength: "<?= sprintf(_("form.field.least"), 1); ?>",
							maxlength: "<?= sprintf(_("form.field.max"), 10000); ?>"
						}
					}
				});
			});
		</script>
</body>

</html>
