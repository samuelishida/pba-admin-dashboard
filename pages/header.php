<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Tell the browser to be responsive to screen width -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- Favicon icon -->
		<link rel="icon" type="image/png" href="<?=$_ENV["hostname"] ?>/admin/assets/images/favicon.png">
		<title><?= $_ENV["GLO_APP_NAME"]; ?></title>
		<?php require $_ENV["GLO_ADMIN_PAGES"]."/css.php"; ?>
	</head>
	<body class="fix-header fix-sidebar">
	<?php
	if (isset($_SESSION["user"])){
	?>
		<form id="app_logout" name="logout" action="/api/v1/user/logout" method="post">
			<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
			<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
		</form>
	<?php
	}
	?>
	<!-- Preloader - style you can find in spinners.css -->
	<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
		</svg>
	</div>
	<!-- Main wrapper	-->
	<div id="main-wrapper">
		<!-- header header	-->
		<div class="header">
			<nav class="navbar top-navbar navbar-expand-md navbar-light">
				<!-- Logo -->
				<div class="navbar-header">
					<a class="navbar-brand" href="/home">
						<!-- Logo icon -->
						<b><img src="/admin/assets/images/logo_icon.svg" alt="homepage" style="width: 2em;" class="dark-logo" /></b>
						<!--End Logo icon -->
						<!-- Logo text -->
						<span>
							<img src="/admin/assets/images/logo_text.svg" alt="homepage" style="width: 4em;" class="dark-logo" />
						</span>
					</a>
				</div>
				<!-- End Logo -->
				<div class="navbar-collapse">
					<!-- toggle and nav items -->
					<ul class="navbar-nav mr-auto mt-md-0">
						<!-- This is	-->
						<li class="nav-item">
							<a class="nav-link nav-toggler hidden-md-up text-muted	" href="javascript:void(0)"><i class="fas fa-bars"></i></a>
						</li>
						<li class="nav-item m-l-10">
							<a class="nav-link sidebartoggler hidden-sm-down text-muted	" href="javascript:void(0)"><i class="fas fa-bars"></i></a>
						</li>
						<!-- Messages -->
						<li class="nav-item dropdown mega-dropdown">
							<a class="nav-link dropdown-toggle text-muted	" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bug"></i></a>
							<div class="dropdown-menu ">
								<ul class="mega-dropdown-menu row">
									<li class="col-lg-9 m-b-30">
										<h4 class="m-b-20"><i class="fa fa-bug"></i> <?= _("system.bug.report"); ?>! </h4>
										<!-- Contact -->
										<form name="report_bug" id="report_bug" action="/api/v1/issue" class="validate form-horizontal form-material" >
											<input type="hidden" id="report_type_bug" name="type" value="Bug" />
											<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
											<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
											<div class="form-group">
												<div class="col-md-12">
													<input type="text" class="form-control" id="report_bug_title" name="title" placeholder="<?= _("system.bug.title") ?>" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<textarea class="form-control" id="report_bug_description" name="description" rows="5" style="height: 5em;" placeholder="<?= _("system.bug.description") ?>"></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-12 text-right">
													<button type="submit" class="btn btn-info"><?= _("system.bug.submit") ?></button>
												</div>
											</div>
										</form>
									</li>
									<li class="col-lg-3 m-b-30">
										<h4 class="m-b-20">... <?= _("system.bug.what_is") ?>?</h4>
										<p>
											<?= _("system.bug.what_is.p1"); ?>
											<!-- Bug é um termo em inglês que se refere a uma falha em algum sistema.
											Sendo assim, para podermos melhorar nosso sistema, primeiro precisamos saber quais bugs <i class="fa fa-bug"></i> ─ ou falhas ─ ele apresenta. -->
										</p>
										<p>
											<?= _("system.bug.what_is.p2"); ?>
											<!-- Para isso, reporte seu bug no formulário ao lado para que nossa equipe possa corrigí-lo. Descreva de forma detalhada cada um dos passos que você fez no sistema até a falha acontecer.
											Assim, nós vamos refazer seus passos até encontrar o seu bug <i class="fa fa-bug"></i> e corrigí-lo. -->
										</p>
										<p>
											<?= _("system.bug.thanks"); ?> <!--Desde já, muito obrigado pela sua ajuda!-->
										</p>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item dropdown mega-dropdown">
							<a class="nav-link dropdown-toggle text-muted	" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-lightbulb"></i></a>
							<div class="dropdown-menu ">
								<ul class="mega-dropdown-menu row">
									<li class="col-lg-9 m-b-30">
										<h4 class="m-b-20"><i class="far fa-lightbulb"></i> <?= _("system.feature.request") ?> ! </h4>
										<!-- Contact -->
										<form name="ask_feature" id="ask_feature" action="/api/v1/issue" class="validate form-horizontal form-material" >
											<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
											<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
											<input type="hidden" id="report_type_feature" name="type" value="Feature" />
											<div class="form-group">
												<div class="col-md-12">
													<input type="text" class="form-control" id="ask_feature_title" name="title" placeholder="<?= _("system.feature.title") ?>" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<textarea class="form-control" name="description" id="ask_feature_description" rows="5" style="height: 5em;" placeholder="<?= _("system.feature.description") ?>"></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-12 text-right">
													<button type="submit" class="btn btn-info"><?= _("system.feature.submit") ?></button>
												</div>
											</div>
										</form>
									</li>
									<li class="col-lg-3 m-b-30">
										<h4 class="m-b-20">... <?= _("system.feature.what_is") ?>?</h4>
										<p>
											<?= _("system.feature.what_is.p1");?>
											<!-- Feature é o termo utilizado para se referir a funcionalidades do sistema.
											Sendo assim, para podermos melhorar nosso sistema, queremos ouvir de você o que você gostaria que ele tivesse <i class="far fa-lightbulb"></i>. Fique a vontade para compartilhar conosco suas idéias ─ por mais malucas que possam parecer, às vezes elas podem ser algo sensacional! -->
										</p>
										<p>
											<?= _("system.feature.what_is.p1"); ?>
											<!-- Para isso, use o formulário ao lado para descrever o que você gostaria com o máximo de detalhes que conseguir: assim que nossa equipe receber sua requisição, avaliaremos sua dificuldade técnica e sua relevância para todos usuários e, caso sua feature <i class="far fa-lightbulb"></i> seja aprovada, em breve você verá ela disponível no sistema. -->
										</p>
										<p>
											<?= _("system.feature.thanks"); ?> <!-- Desde já, muito obrigado pela sua ajuda! <i class="fa fa-smile-o"></i> -->
										</p>
									</li>
								</ul>
							</div>
						</li>
					</ul>
					<!-- User profile and notifications -->
					<?php
						$system_notifications = $_SESSION["user"]->getUnreadNotifications(Notification::system);
					?>
					<ul class="navbar-nav my-lg-0">
						<!-- Search -->
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle text-muted text-muted	" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-wrench"></i>
								<?php
									if (count($system_notifications)>0){
										?>
											<div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
										<?php
									}
								?>
							</a>
							<form id="read_notification" name="read-notification" action="/api/v1/user/notification/read" method="post">
								<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
								<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
								<input id="user_notification" type="hidden" name="user_notification" value="" />
							</form>
							<div class="dropdown-menu dropdown-menu-right mailbox ">
								<ul>
									<li>
										<div class="drop-title"><?= _("notification.system.title"); ?></div>
									</li>
									<li>
										<div class="message-center">
											<?php
												if (count($system_notifications)>0){
													foreach ($system_notifications as $system_notification){
														switch ($system_notification->getNotification()->getId()){
															case Notification::new_notification_id:
																?>
																<a href="#" class="user-notification">
																	<input name="user_notification_id" type="hidden" value="<?= $system_notification->getId(); ?>" />
																	<div class="btn btn-info btn-circle m-r-10">
																		<i class="fas fa-file-alt"></i>
																	</div>
																	<div class="mail-contnet">
																		<h5><?= $system_notification->getNotification()->getTitle(); ?></h5> <span class="mail-desc"><?= $system_notification->getNotification()->getDescription(); ?></span> <span class="time"><?= StringUtils::timeElapsed($system_notification->getCreatedAt()); ?></span>
																	</div>
																</a>
																<?php
															break;
														}
													}
												}else{
													?>
													<a href="#" class="">
														<div class="btn btn-info btn-circle m-r-10">
															<i class="fas fa-thumbs-up"></i>
														</div>
														<div class="mail-contnet">
															<h5><?= _("notification.empty.title"); ?></h5> <span class="mail-desc"><?= _("notification.empty.description"); ?>.</span>
														</div>
													</a>
													<?php
												}
											?>
										</div>
									</li>
									<li>
										<a class="nav-link text-center" href="javascript:void(0);"> <strong><?= _("notification.check.all"); ?></strong> <i class="fa fa-angle-right"></i> </a>
									</li>
								</ul>
							</div>
						</li>
						<!-- End Messages -->
						<!-- Profile -->
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle text-muted	" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?= $_SESSION["user"]->showProfilePicture(); ?>" alt="user" class="profile-pic" /></a>
							<div class="dropdown-menu dropdown-menu-right ">
								<ul class="dropdown-user">
									<li><a href="/users/view/<?= $_SESSION["user"]->getId(); ?>"><i class="ti-user"></i> <?= _("user.profile") ?></a></li>
									<li><a href="/users/edit/<?= $_SESSION["user"]->getId(); ?>"><i class="ti-settings"></i> <?= _("user.settings") ?></a></li>
									<li><a id="logout" href="#"><i class="fa fa-power-off"></i> <?= _("user.logout") ?></a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- End header header -->
		<!-- Left Sidebar	-->
		<div class="left-sidebar">
			<!-- Sidebar scroll-->
			<div class="scroll-sidebar">
				<!-- Sidebar navigation-->
				<nav class="sidebar-nav">
					<ul id="sidebarnav">
						<li class="nav-devider"></li>
						<li class="nav-label"><?= _("menu.home") ?></li>
						<li>
							<a class="has-arrow	" href="#" aria-expanded="false"><i class="fas fa-tachometer-alt"></i><span class="hide-menu"><?= _("menu.dashboard") ?> <!--<span class="label label-rouded label-primary pull-right"></span> --></span></a>
							<ul aria-expanded="false" class="collapse">
								<li><a href="/home"><?= _("menu.overview") ?></a></li>
							</ul>
						</li>
						<li>
							<a class="has-arrow	" href="#" aria-expanded="false"><i class="fa fa-wrench"></i><span class="hide-menu"><?= _("menu.admin") ?></span></a>
							<ul aria-expanded="false" class="collapse">
								<li><a href="/users"><?= _("menu.users") ?></a></li>
								<li><a href="/emails"><?= _("menu.emails") ?></a></li>
							</ul>
						</li>
					</ul>
				</nav>
				<!-- End Sidebar navigation -->
			</div>
		<!-- End Sidebar scroll-->
		</div>
		<!-- End Left Sidebar	-->
		<!-- Page wrapper	-->
		<div class="page-wrapper">
			<!-- Bread crumb -->
			<div class="row page-titles">
				<div class="col-md-5 align-self-center">
					<h3 class="text-primary"><?= $primary_title; ?></h3>
				</div>
				<div class="col-md-7 align-self-center">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/home"><?= _("menu.dashboard") ?></a></li>
						<?php
							foreach ($breadcrumb as $bread => $crumb){
								print '<li class="breadcrumb-item"><a href="'.$crumb.'">'.$bread.'</a></li>';
							}
						?>
					</ol>
				</div>
			</div>
			<!-- End Bread crumb -->
			<!-- Container fluid	-->
			<div class="container-fluid">
