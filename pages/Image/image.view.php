<?php require $_ENV["GLO_ADMIN_PAGES"]."/header.php"; ?>
	<!-- Start Page Content -->
	<div class="row">
			<!-- Column -->
			<div class="col-lg-12">
					<div class="card">
							<div class="card-body">
									<div class="card-two">
										<div class="text-right">
											<button value="/images/edit/<?= $image->getId(); ?>" class="btn btn-dark btn-outline m-b-10 link"><i class="fas fa-pencil-alt"></i></button>
										</div>
										<img src="<?= $image->getHighResolution();?>" class="page-thumbnail" />
										<h3><?= $image->getTitle(); ?></h3>
										<p class="text-center">
											<?= $image->getFilename(); ?></a>
										</p>
										<div class="desc">
												<?= $image->getDescription(); ?>
										</div>
										<div class="clear"></div>
									</div>
							</div>
					</div>
			</div>
			<!-- Column -->
	</div>
	<!-- End Page Content -->
<?php require $_ENV["GLO_ADMIN_PAGES"]."/footer.php"; ?>
