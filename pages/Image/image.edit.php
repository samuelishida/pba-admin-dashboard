<?php require $_ENV["GLO_ADMIN_PAGES"]."/header.php"; ?>
	<!-- Start Page Content -->
	<div class="row">
			<!-- Column -->
			<div class="col-lg-12">
					<div class="card">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs profile-tab" role="tablist">
									<li class="nav-item active"> <a class="nav-link active show" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
									<!--second tab-->
									<div class="tab-pane active" id="settings" role="tabpanel">
											<div class="card-body">
													<form name="image" id="image" action="/api/v1/image/edit" method="post" class="validate form-horizontal form-material">
															<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
															<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
															<input type="hidden" id="image_id" name="id" value="<?=$image->getId(); ?>" />
															<div class="form-group">
																<div class="col-md-12">
																	<img src="<?= $image->getHighResolution();?>" class="page-thumbnail" />
																</div>
																<div class="col-md-12">
																		<input id="image_filename" name="filename" type="text" placeholder="<?= _("image.filename"); ?>" value="<?= $image->getFilename(); ?>" class="form-control form-control-line text-center" disabled>
																</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12"><?= _("table.title") ?></label>
																	<div class="col-md-12">
																		<input id="image_title" type="text" name="title" placeholder="<?= _("image.title"); ?>" value="<?= $image->getTitle(); ?>" class="form-control form-control-line">
																	</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12"><?= _("table.description") ?></label>
																	<div class="col-md-12">
																		<textarea id="image_description" name="description" placeholder="<?= _("image.description.placeholder"); ?>" rows="10" class="form-control textarea_editor" style="height: 10em;"><?= $image->getDescription(); ?></textarea>
																	</div>
															</div>
															<div class="form-group">
																	<div class="col-sm-12 text-right">
																			<input type="button" name="cancel" value="Cancel" class="btn btn-danger" onclick="window.history.back();"/>
																			<button class="btn btn-success"><?= _("image.update") ?></button>
																	</div>
															</div>
													</form>
											</div>
									</div>
							</div>
					</div>
			</div>
			<!-- Column -->
	</div>
	<!-- End Page Content -->
<?php require $_ENV["GLO_ADMIN_PAGES"]."/footer.php"; ?>
<script>
$('body').on("treat", "#image", function(event, response){
	switch (response.status){
		case 200:
			/* HTTP 200: OK */
			if (response.data.success == true){
				window.location.href = "/images/view/<?php print $image->getId(); ?>";
			}else{
				toastr.error('Tente novamente. Se não funcionar, entre em contato com o administrador.','<?= _("image.upload.failed") ?>',{
					"positionClass": "toast-top-full-width",
					timeOut: 5000,
					"closeButton": true,
					"debug": false,
					"newestOnTop": true,
					"progressBar": true,
					"preventDuplicates": true,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut",
					"tapToDismiss": false
				})
			}
		break;
		case 400:
			/* HTTP 400: Bad request */
			toastr.error('<?= _("form.field.invalid") ?>','<?= _("image.upload.failed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 403:
			/* HTTP 403: Forbidden */
			toastr.error('<?= _("system.action.not_allowed") ?>','<?= _("image.upload.failed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 404:
			/* HTTP 404: Not found */
			toastr.error('<?= _("system.action.inexistent") ?>','<?= _("image.upload.failed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 500:
			/* HTTP 500: Internal server error */
			toastr.error('<?= _("system.server.unavailable"); ?>','<?= _("image.upload.failed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		default:
			/* Something is wrong */
			toastr.error('<?= _("system.unknown.error") ?>', '<?= _("image.upload.failed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
	}
});

$(document).ready(function(){
	$("#image").validate({
		ignore: [],
		errorClass: "invalid-feedback animated fadeInDown",
		errorElement: "div",
		errorPlacement: function(e, a) {
			jQuery(a).parents(".form-group > div").append(e)
		},
		highlight: function(e) {
			jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
		},
		success: function(e) {
			jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
		},
		submitHandler:
			function(form) {
				$(form).submitHandler();
				return false;
		},
		rules: {
			title: {
				required: true,
				minlength: 3,
				maxlength: 255
			},
			description: {
				required: true,
				minlength: 5,
				maxlength: 500
			},
		},
		messages: {
			title: {
				required: <?= _("messages.required") ?>,
				minlength: <?= _("messages.minlength_3") ?>,
				maxlength: <?= _("messages.maxlength_255") ?>
			},
			description: {
				required: <?= _("messages.required") ?>,
				minlength: <?= _("messages.minlength_5") ?>,
				maxlength: <?= _("messages.maxlength_500") ?>
			}
		}
	});
});
</script>
