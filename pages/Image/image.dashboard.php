<?php require $_ENV["GLO_ADMIN_PAGES"]."/header.php"; ?>
	<!-- Start Page Content -->
	<div class="row">
		<div class="col-lg-12 text-right">
			<button value="/images/new" class="btn btn-primary m-b-10 link"><i class="fa fa-plus"></i><?= _("image.create") ?></button>
		</div>
	</div>
	<div class="row d-flex align-items-stretch">
	<?php
		if ($images && count($images)>0){
			foreach ($images as $image){
			?>
				<div class="col-lg-6 d-flex align-items-stretch">
					<div class="card" style="width: 100%;">
						<div class="card-body">
							<div class="card-two">
								<img src="<?= $image->getHighResolution();?>" class="page-thumbnail" />
								<h3><?= $image->getTitle(); ?></h3>
								<p class="text-center">
									<?= $image->getFilename(); ?>
								</p>
								<hr>
								<div class="desc">
									<p class="text-center"><?= $image->getDescription(); ?>
									</p>
								</div>
								<div class="clear"></div>
								<div class="text-center">
									<button value="/images/view/<?= $image->getId(); ?>" class="btn btn-dark btn-outline m-b-10 link"><i class="fa fa-search"></i></button>
									<button value="/images/edit/<?= $image->getId(); ?>" class="btn btn-dark btn-outline m-b-10 link"><i class="fas fa-pencil-alt"></i></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php
			}
		}
	?>
	</div>
	<div class="row">
			<div class="col-lg-12">
					<div class="card">
							<div class="card-body">
									<h4 class="card-title"><?= _("image.list") ?></h4>
									<h6 class="card-subtitle"><?= _("image.list_all") ?></h6>
                  <div class="table-responsive m-t-40">
										<input id="image_table_csrf_name" type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
										<input id="image_table_csrf_key" type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
										<table id="images_table" style="width: 100%;" class="datatable table table-bordered table-striped">
											<thead>
												<tr>
													<th><?= _("table.title") ?></th>
													<th><?= _("table.description") ?></th>
													<th><?= _("table.date") ?></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
							</div>
					</div>
			</div>
<!-- End Page Content -->
<?php require $_ENV["GLO_ADMIN_PAGES"]."/footer.php"; ?>
<script>
	$(document).ready(function(){
		$("#images_table").DataTable({
			"language": {
				"processing": '<i class="fa fa-spinner fa-pulse"></i> Processing...'
			},
			"dom": '<"top"lf>rt<"bottom"pi><"clear">',
			"columnDefs": [
				{ "searchable": false, "orderable": false, "targets": 3 },
				{ "responsivePriority": 1, "targets": 0},
				{ "responsivePriority": 2, "targets": 3},
				{ "responsivePriority": 3, "targets": 1}
			],
			"lengthChange": true,
			"processing": true,
			"serverSide": true,
			"responsive": true,
			"ajax": {
				"url": "/api/v1/image/datatable",
				"contentType": "application/json",
				"type": "POST",
				"data": function (d) {
					return JSON.stringify($.extend( {}, d, {"csrf_name" : $("#image_table_csrf_name").val(),"csrf_value" : $("#image_table_csrf_key").val()}));
				},
				"dataFilter": function (d){
					var response = JSON.parse(d);
					$(this).updateCSRF(response.csrf);
					return JSON.stringify(response.data);
				},
				"error": function (d){
					$("#message").html('<div class="error"><p><i class="fa fa-times-circle"></i> Something went wrong while retrieving table data. Try again later.</p></div>');
					$(".dataTables_processing").addClass("error-message");
					$(".dataTables_processing").html('<i class="fa fa-times"></i> Error');
				}
			}
		});
	});
</script>
