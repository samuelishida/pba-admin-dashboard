<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Tell the browser to be responsive to screen width -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- Favicon icon -->
		<link rel="icon" type="image/png" href="<?=$_ENV["hostname"] ?>/admin/assets/images/favicon.png">
		<title><?= $_ENV["GLO_APP_NAME"]; ?></title>
		<?php require $_ENV["GLO_ADMIN_PAGES"]."/css.php"; ?>
	</head>

	<body class="fix-header fix-sidebar">
		<!-- Preloader - style you can find in spinners.css -->
		<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50">
				<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
		</div>
		<!-- Main wrapper  -->
		<div id="main-wrapper">
			<div class="unix-login">
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-lg-4">
							<div class="login-content card">
								<div class="form-validation">
									<div class="login-form form-valide">
										<div class="row mb-5">
											<span style="margin-left: auto;margin-right: auto;display:block;">
												<img src="/admin/assets/images/logo_icon.svg" alt="homepage" style="width: 5em;" class="dark-logo" />
												<img src="/admin/assets/images/logo_text.svg" alt="homepage" style="width: 10em;" class="dark-logo" />
											</span>
										</div>
										<form id="login" name="login" action="/api/v1/user/auth" method="post" class="validate">
											<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
											<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
											<div class="form-group">
												<div>
													<input type="text" id="login_username" name="username" class="form-control" autofocus 	tabindex="1" placeholder="<?= _("user.login.username"); ?>">
												</div>
											</div>
											<div class="form-group">
												<div>
													<input type="password" id="login_password" name="password" class="form-control" tabindex="2"	 placeholder="<?= _("user.login.password"); ?>">
												</div>
											</div>
											<div class="checkbox">
												<label class="pull-right">
													<a href="/reset"><?= _("user.login.reset_password"); ?></a>
												</label>
											</div>
											<button type="submit" tabindex="3" class="btn btn-primary btn-flat m-b-30 m-t-30"><?= _("user.login.sign_in"); ?></button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- End Wrapper -->
		<!-- All Jquery -->
		<?php require $_ENV["GLO_ADMIN_PAGES"]."/script.php"; ?>
		<script>
			$(document).ready(function(){
				<?php
					if (isset($action) && $action!=null){
						switch ($action){
							case "reset":
								?>
								toastr.warning('<?= _("user.reset_password.check_your_inbox"); ?>', '<?= _("user.reset_password.email_sent"); ?>',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false
								})
								<?php
							break;
							case "success":
								?>
								toastr.success('<?= _("user.reset_password.login"); ?>', '<?= _("user.reset_password.success"); ?> ',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false
								})
								<?php
							break;
							case "confirmed":
								?>
								toastr.success('<?= _("user.email_confirmation.confirmed"); ?>', '<?= _("user.email_confirmation.success"); ?>',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false
								})
								<?php
							break;
						}
					}
				?>
				$("#login").on("treat", function(event, response){
					switch (response.status){
						case 200:
							/* HTTP 200: OK */
							if (response.data.success == true){
								window.location.href = "/home";
							}else{
								toastr.error('<?= _("user.authentication.invalid_credentials"); ?>', '<?= _("user.authentication.failed"); ?>',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false
								})
							}
						break;
						case 400:
							/* HTTP 400: Bad request */
							toastr.error('<?= _("form.field.invalid"); ?>', '<?= _("form.invalid"); ?>',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false

								})
						break;
						case 403:
							/* HTTP 403: Forbidden */
							toastr.error('<?= _("system.action.not_allowed"); ?>','<?= _("system.action.forbidden"); ?>',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false

								})
						break;
						case 404:
							/* HTTP 404: Not found */
							toastr.error('<?= _("system.action.inexistent") ?>' ,'<?= _("system.action.unavailable"); ?>',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false

								})
						break;
						case 500:
							/* HTTP 500: Internal server error */
							toastr.error('<?= _("system.server.unavailable"); ?>','<?= _("system.server.failure"); ?>',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false

								})
						break;
					}
				});
				$("#login").validate({
					ignore: [],
					errorClass: "invalid-feedback animated fadeInDown",
					errorElement: "div",
					errorPlacement: function(e, a) {
						jQuery(a).parents(".form-group > div").append(e)
					},
					highlight: function(e) {
						jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
					},
					success: function(e) {
						jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
					},
					submitHandler:
						function(form) {
							$(form).submitHandler();
							return false;
					},
					rules: {
						username: {
							required: true
						},
						password: {
							required: true
						}
					},
					messages: {
						username: {
							required: "<?= _("form.field.required"); ?>"
						},
						password: {
							required: "<?= _("form.field.required"); ?>"
						}
					}
				});
			});
		</script>
	</body>
</html>
