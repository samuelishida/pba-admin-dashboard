<?php require $_ENV["GLO_ADMIN_PAGES"]."/header.php"; ?>
	<form name="image_selection" id="image_selection" action="/api/v1/image/list" method="post" class="hidden">
		<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
		<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
	</form>
	<div id="image_form_csrf">
		<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
		<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
	</div>
	<!-- Start Page Content -->
	<div class="row">
			<!-- Column -->
			<div class="col-lg-12">
					<div class="card">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs profile-tab" role="tablist">
									<li class="nav-item active"> <a class="nav-link active show" data-toggle="tab" href="#settings" role="tab">Profile</a> </li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
									<!--second tab-->
									<div class="tab-pane active" id="settings" role="tabpanel">
											<div class="card-body">
													<form name="user" id="user" action="/api/v1/user" method="post" class="validate form-horizontal form-material">
															<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
															<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
															<div class="form-group">
																	<label class="col-md-12"><?= _("user.profile.picture") ?></label>
																	<div class="col-md-12">
																		<input type="hidden" id="user_profile_picture" name="profile_picture" value="" />
																		<div class="avatar">
																			<img id="user_profile_picture_avatar" src="<?= User::defaultProfilePicture(); ?>" class="profile-thumbnail" />
																			<p id="user_profile_picture_empty" class="text-center"><?= _("user.profile.no_picture") ?></p>
																		</div>
																	</div>
																	<div class="col-md-12 text-center">
																		<button id="change_picture" class="btn btn-info m-b-10"><?= _("user.profile.change_picture") ?></button>
																		<button id="remove_profile_picture" class="btn btn-danger m-b-10 hidden"><?= _("user.profile.remove_picture") ?></button>
																	</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12"><?= _("user.login.username"); ?></label>
																	<div class="col-md-12">
																		<input id="user_username" type="text" name="username" placeholder="john.Doe" value="" class="form-control form-control-line">
																	</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12"><?= _("user.login.fullname") ?></label>
																	<div class="col-md-12">
																			<input id="user_name" name="name" type="text" placeholder="John Doe" value="" class="form-control form-control-line">
																	</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12"><?= _("user.login.email") ?></label>
																	<div class="col-md-12">
																			<input id="user_email" type="email" name="email" type="email" placeholder="example@email.com" value="" class="form-control form-control-line">
																	</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12"><?= _("user.login.password"); ?></label>
																	<div class="col-md-12">
																			<input	id="user_password" type="password" name="password" value="" class="form-control form-control-line">
																	</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12"><?= _("user.profile.biography"); ?></label>
																	<div class="col-md-12">
																		<textarea id="user_bio" name="bio" placeholder="<?= _("user.profile.biography.placeholder") ?>" rows="10" class="form-control textarea_editor" style="height: 10em;"></textarea>
																	</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12">Linkedin</label>
																	<div class="col-md-12">
																		<input id="user_linkedin" type="text" name="linkedin" placeholder="Linkedin" value="" class="form-control form-control-line" />
																	</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12">Facebook</label>
																	<div class="col-md-12">
																		<input id="user_facebook" type="text" name="facebook" placeholder="Facebook" value="" class="form-control form-control-line" />
																	</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12">Twitter</label>
																	<div class="col-md-12">
																		<input id="user_twitter" type="text" name="twitter" placeholder="Twitter" value="" class="form-control form-control-line" />
																	</div>
															</div>
															<div class="form-group">
																	<label class="col-md-12">Youtube</label>
																	<div class="col-md-12">
																		<input id="user_youtube" type="text" name="youtube" placeholder="Youtube" value="" class="form-control form-control-line" />
																	</div>
															</div>
															<h5><?= _("user.notifications.settings") ?></h5>
															<hr/>
															<div class="form-group">
																<div class="col-md-12">
																	<div class="pretty p-switch">
																		<input id="user_notify_system_events" name="notify_system_events" type="checkbox" value="1" />
																		<div class="state p-success">
																			<label for="notify_system_events"><?= _("user.notify_system_events") ?></label>
																		</div>
																	</div>
																</div>
															</div>
															<div class="form-group">
																	<div class="col-sm-12 text-right">
																			<input type="button" name="cancel" value="<?= _("system.cancel") ?>" class="btn btn-danger" onclick="window.history.back();"/>
																			<button class="btn btn-success"><?= _("user.profile.create") ?></button>
																	</div>
															</div>
													</form>
											</div>
									</div>
							</div>
					</div>
			</div>
			<!-- Column -->
	</div>
	<!-- End Page Content -->
<?php require $_ENV["GLO_ADMIN_PAGES"]."/footer.php"; ?>
<script>
$('body').on("click", '.upload-image', function(e){
		e.preventDefault();
		Swal.fire({
			title: 'Upload Picture',
			showCancelButton: true,
			html:
					'<form name="image" id="image" action="/api/v1/image" method="post" class="validate upload form-horizontal form-material">'+
						$("#image_form_csrf").html()+
						'<div class="form-group text-left">'+
							'<div class="col-md-12">'+
								'<label>Title</label>'+
							'</div>'+
							'<div class="col-md-12">'+
								'<input id="image_title" type="text" name="title" placeholder="<?= _("image.title") ?>" class="form-control form-control-line" required />'+
							'</div>'+
						'</div>'+
						'<div class="form-group text-left">'+
							'<div class="col-md-12">'+
								'<label>Description</label>'+
							'</div>'+
							'<div class="col-md-12">'+
								'<textarea id="image_description" name="description" placeholder="<?= _("image.description.alt_text") ?>" class="form-control form-control-line" required ></textarea>'+
							'</div>'+
						'</div>'+
						'<div class="form-group text-left">'+
							'<div class="col-md-12">'+
								'<label>Image</label>'+
							'</div>'+
							'<div class="col-md-12">'+
								'<input id="image_file" type="file" name="image_file" placeholder="<?= _("image") ?>" class="" required />'+
							'</div>'+
						'</div>'+
					'</form>',
			focusConfirm: false,
			reverseButtons: true,
			preConfirm: function(){
				$("#image").on("treat", function(event, response){
					switch (response.status){
						case 200:
							/* HTTP 200: OK */
							if (response.data.success){
								$("#user_profile_picture").val(response.data.data.id);
								$("#user_profile_picture_avatar").attr("src", response.data.data.high_resolution);
								$("#user_profile_picture_empty").hide();
								$("#user_profile_picture_avatar").show();
								$("#remove_profile_picture").show();
							}else{
								toastr.error('<?= _("image.title.change") ?>','<?= _("image.upload.failed") ?>',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false
								})
							}
						break;
						case 400:
							/* HTTP 400: Bad request */
							toastr.error('<?= _("form.field.invalid"); ?>','<?= _("system.upload.failed") ?>',{
								"positionClass": "toast-top-full-width",
								timeOut: 5000,
								"closeButton": true,
								"debug": false,
								"newestOnTop": true,
								"progressBar": true,
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"tapToDismiss": false
							})
						break;
						case 403:
							/* HTTP 403: Forbidden */
							toastr.error('<?= _("file.upload.not_allowed") ?>','<?= _("system.upload.failed") ?>',{
								"positionClass": "toast-top-full-width",
								timeOut: 5000,
								"closeButton": true,
								"debug": false,
								"newestOnTop": true,
								"progressBar": true,
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"tapToDismiss": false
							})
						break;
						case 404:
							/* HTTP 404: Not found */
							toastr.error('<?= _("file.upload.not_found") ?>','<?= _("system.upload.failed") ?>',{
								"positionClass": "toast-top-full-width",
								timeOut: 5000,
								"closeButton": true,
								"debug": false,
								"newestOnTop": true,
								"progressBar": true,
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"tapToDismiss": false
							})
						break;
						case 500:
							/* HTTP 500: Internal server error */
								toastr.error('<?= _("system.server.unavailable") ?>','<?= _("system.upload.failed") ?>',{
								"positionClass": "toast-top-full-width",
								timeOut: 5000,
								"closeButton": true,
								"debug": false,
								"newestOnTop": true,
								"progressBar": true,
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"tapToDismiss": false
							})
						break;
						default:
							/* Something is wrong */
								toastr.error('<?= _("system.unknown.error") ?>','<?= _("system.upload.failed") ?>',{
								"positionClass": "toast-top-full-width",
								timeOut: 5000,
								"closeButton": true,
								"debug": false,
								"newestOnTop": true,
								"progressBar": true,
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"tapToDismiss": false
							})
						break;
					}
				});
				$("#image").validate({
					ignore: [],
					errorClass: "invalid-feedback animated fadeInDown",
					errorElement: "div",
					errorPlacement: function(e, a) {
						jQuery(a).parents(".form-group > div").append(e)
					},
					highlight: function(e) {
						jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
					},
					success: function(e) {
						jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
					},
					rules: {
						title: {
							required: true,
							minlength: 1,
							maxlength: 255
						},
						description: {
							required: true,
							minlength: 1,
							maxlength: 255
						},
						image_file: {
							required: true,
							extension: "png|jpe?g|gif"
						}
					},
					messages: {
						title: {
							required: <?= _("messages.required") ?>,
							minlength: <?= _("messages.minlength_1") ?>,
							maxlength: <?= _("messages.maxlength_255") ?>
						},
						description: {
							required: <?= _("messages.required") ?>,
							minlength: <?= _("messages.minlength_1") ?>,
							maxlength: <?= _("messages.maxlength_500") ?>
						},
						image_file: {
							required: <?= _("messages.required") ?>,
							extension: <?= _("messages.extension") ?>
						}
					}
				});
				if ($("#image").valid()){
					$("#image").submit();
					return true;
				}
				return false;
			}
		});
		return false;
});
$('body').on("click", '#change_picture', function(e){
		e.preventDefault();
		Swal.fire({
			title: 'Change Profile Picture',
			showCancelButton: true,
			html: '<p>Choose a picture</p><select id="user_profile_picture_selection" class="image-picker-input"></select><p class="m-t-15 m-b-10">or</p><button class="btn btn-info m-b-10 btn upload-image">Upload a new picture</button>',
			focusConfirm: false,
			reverseButtons: true,
			onBeforeOpen: function(){
				$("#image_selection").submit();
			},
			preConfirm: function(){
				$("#user_profile_picture").val($("#user_profile_picture_selection").val());
				$("#user_profile_picture_avatar").attr("src", $("#user_profile_picture_selection option:selected").attr("data-img-src"));
				$("#user_profile_picture_empty").hide();
				$("#user_profile_picture_avatar").show();
				$("#remove_profile_picture").show();
				return true;
			}
		});
		return false;
});
$('body').on("click", "#remove_profile_picture", function(e){
	e.preventDefault;
	$("#user_profile_picture").val(null);
	$("#remove_profile_picture").hide();
	$("#user_profile_picture_avatar").attr("src", "<?= User::defaultProfilePicture(); ?>");
	$("#user_profile_picture_empty").show();
	return false;
});
$('body').on("treat", "#image_selection", function(event, response){
	switch (response.status){
		case 200:
			/* HTTP 200: OK */
			if (response.data.success){
				var images = response.data.data;
				for (var i=0; i<images.length; i++){
					$("#user_profile_picture_selection").append($("<option></option>").attr("value", images[i].id).attr('data-img-src', images[i].high_resolution));
				}
				$("#user_profile_picture_selection").imagepicker();
			}else{
				toastr.error('<?= _("image.upload.failed") ?>','<?= _("system.try.again") ?>',{
					"positionClass": "toast-top-full-width",
					timeOut: 5000,
					"closeButton": true,
					"debug": false,
					"newestOnTop": true,
					"progressBar": true,
					"preventDuplicates": true,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut",
					"tapToDismiss": false

				})
			}
		break;
		case 400:
			/* HTTP 400: Bad request */
			toastr.error('<?= _("image.error") ?>','<?= _("form.field.invalid") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false

			})
		break;
		case 403:
			/* HTTP 403: Forbidden */
			toastr.error('<?= _("image.error") ?>','<?= _("image.not_allowed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false

			})
		break;
		case 404:
			/* HTTP 404: Not found */
			toastr.error('<?= _("image.error") ?>','<?= _("image.inexistent") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false

			})
		break;
		case 500:
			/* HTTP 500: Internal server error */
			toastr.error('<?= _("image.error") ?>','<?= _("system.server.unavailable") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false

			})
		break;
		default:
			/* Something is wrong */
			toastr.error('<?= _("image.error") ?>','<?= _("system.unknown.error") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false

			})
		break;
	}
});
$('body').on("treat", "#image", function(event, response){
	switch (response.status){
		case 200:
			/* HTTP 200: OK */
			if (response.data.success){
				$("#user_profile_picture").val(response.data.data.id);
				$("#user_profile_picture_avatar").attr("src", response.data.data.high_resolution);
				$("#user_profile_picture_empty").hide();
				$("#user_profile_picture_avatar").show();
				$("#remove_profile_picture").show();
			}else{
				toastr.error('<?= _("image.upload.failed") ?>','<?= _("image.title.change") ?>',{
					"positionClass": "toast-top-full-width",
					timeOut: 5000,
					"closeButton": true,
					"debug": false,
					"newestOnTop": true,
					"progressBar": true,
					"preventDuplicates": true,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut",
					"tapToDismiss": false
				})
			}
		break;
		case 400:
			/* HTTP 400: Bad request */
			toastr.error('<?= _("system.upload.failed") ?>','<?= _("form.field.invalid") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 403:
			/* HTTP 403: Forbidden */
			toastr.error('<?= _("system.upload.failed") ?>','<?= _("file.upload.not_allowed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 404:
			/* HTTP 404: Not found */
			toastr.error('<?= _("system.upload.failed") ?>','<?= _("file.upload.inexistent") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 500:
			/* HTTP 500: Internal server error */
			toastr.error('<?= _("system.upload.failed") ?>','<?= _("system.server.unavailable") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		default:
			/* Something is wrong */
			toastr.error('<?= _("system.upload.failed") ?>','<?= _("system.unknown.error") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
	}
});
$('body').on("treat", "#user", function(event, response){
	switch (response.status){
		case 200:
			/* HTTP 200: OK */
			if (response.data.success == true){
				window.location.href = "/users/view/"+ response.data.data.id;
			}else{
				toastr.error('<?= _("user.upload.failed") ?>','<?= _("system.try.again") ?>',{
					"positionClass": "toast-top-full-width",
					timeOut: 5000,
					"closeButton": true,
					"debug": false,
					"newestOnTop": true,
					"progressBar": true,
					"preventDuplicates": true,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut",
					"tapToDismiss": false
				})
			}
		break;
		case 400:
			/* HTTP 400: Bad request */
			toastr.error('<?= _("user.upload.failed") ?>','<?= _("form.field.invalid") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 403:
			/* HTTP 403: Forbidden */
			toastr.error('<?= _("user.upload.failed") ?>','<?= _("system.action.not_allowed") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 404:
			/* HTTP 404: Not found */
			toastr.error('<?= _("user.upload.failed") ?>','<?= _("system.action.inexistent") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		case 500:
			/* HTTP 500: Internal server error */
			toastr.error('<?= _("system.upload.failed") ?>',''<?= _("system.server.unavailable") ?>'',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
		default:
			/* Something is wrong */
			toastr.error('<?= _("system.upload.failed") ?>','<?= _("system.unknown.error") ?>',{
				"positionClass": "toast-top-full-width",
				timeOut: 5000,
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"tapToDismiss": false
			})
		break;
	}
});

$(document).ready(function(){
	$("#user").validate({
		ignore: [],
		errorClass: "invalid-feedback animated fadeInDown",
		errorElement: "div",
		errorPlacement: function(e, a) {
			jQuery(a).parents(".form-group > div").append(e)
		},
		highlight: function(e) {
			jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
		},
		success: function(e) {
			jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
		},
		submitHandler:
			function(form) {
				$(form).submitHandler();
				return false;
		},
		rules: {
			name: {
				required: true,
				minlength: 3,
				maxlength: 255
			},
			username: {
				required: true,
				minlength: 3,
				maxlength: 30
			},
			password: {
				required: true,
				minlength: 6,
				maxlength: 255
			}
		},
		messages: {
			name: {
				required: <?= _("messages.required") ?>,
				minlength: <?= _("messages.minlength_3") ?>,
				maxlength: <?= _("messages.maxlength_255") ?>
			},
			username: {
				required: <?= _("messages.required") ?>,
				minlength: <?= _("messages.minlength_3") ?>,
				maxlength: <?= _("messages.maxlength_30") ?>
			},
			password: {
				required: <?= _("messages.required") ?>,
				minlength:<?= _("messages.minlength_6") ?>,
				maxlength: <?= _("messages.maxlength_255") ?>
			}
		}
	});
});
</script>
