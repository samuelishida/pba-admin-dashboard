<?php require $_ENV["GLO_ADMIN_PAGES"]."/header.php"; ?>
	<!-- Start Page Content -->
	<div class="row">
			<!-- Column -->
			<div class="col-lg-12">
					<div class="card">
							<div class="card-body">
									<div class="card-two">
										<div class="text-right">
											<button value="/users/edit/<?= $user->getId(); ?>" class="btn btn-dark btn-outline m-b-10 link"><i class="fas fa-pencil-alt"></i></button>
										</div>
											<header>
													<div class="avatar">
														<img src="<?= $user->showProfilePicture(); ?>"/>
													</div>
											</header>

											<h3><?= $user->getName() ?></h3>
											<div class="desc">
													<?= $user->getBio(); ?>
											</div>
											<div class="contacts">
												<a href="<?= $user->getFacebook(); ?>"><i class="fab fa-facebook"></i></a>
												<a href="<?= $user->getLinkedin(); ?>"><i class="fab fa-linkedin"></i></a>
												<a href="<?= $user->getTwitter(); ?>"><i class="fab fa-twitter"></i></a>
												<a href="<?= $user->getYoutube(); ?>"><i class="fab fa-youtube"></i></a>
											</div>
											<div class="clear"></div>
									</div>
							</div>
					</div>
			</div>
			<!-- Column -->
			<!-- Column -->
			<div class="col-lg-12">
					<div class="card">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs profile-tab" role="tablist">
									<li class="nav-item active"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab"><?= _("user.profile") ?></a> </li>
									<!-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li> -->
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
									<!--second tab-->
									<div class="tab-pane active" id="profile" role="tabpanel">
											<div class="card-body">
													<div class="row">
														<div class="col-md-3 col-xs-6 b-r"> <strong><?= _("user.username") ?></strong>
																<br>
																<p class="text-muted"><?= $user->getUsername(); ?></p>
														</div>
															<div class="col-md-3 col-xs-6 b-r"> <strong><?= _("user.fullname") ?></strong>
																	<br>
																	<p class="text-muted"><?= $user->getName(); ?></p>
															</div>
															<div class="col-md-3 col-xs-6 b-r"> <strong><?= _("user.email") ?></strong>
																	<br>
																	<p class="text-muted">
																		<?php
																			if ($user->getEmailConfirmed()){
																				?>
																				<span class="green"><i class="fa fa-check-circle"></i></span>
																				<?php
																			}else{
																				?>
																				<span class="red"><i class="fa fa-times-circle"></i></span>
																				<?php
																			}
																		?>
																		<?= $user->getEmail(); ?>
																	</p>
															</div>
													</div>
													<hr>
													<br/>
													<h5><?= _("user.notifications_settings") ?></h5>
													<hr>
													<div class="row">
														<div class="col-md-3 col-xs-3"> <strong><?= _("user.system_notifications") ?></strong>
																<br>
																<p class="text-muted">
																	<?php
																		if ($user->getNotifySystemEvents()){
																			?>
																			<span class="green"><i class="fa fa-check-circle"></i> <?= _("user.subscribed") ?></span>
																			<?php
																		}else{
																			?>
																			<span class="red"><i class="fa fa-times-circle"></i> <?= _("user.unsubscribed") ?></span>
																			<?php
																		}
																	?>
																</p>
														</div>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
			<!-- Column -->
	</div>
	<!-- End Page Content -->
<?php require $_ENV["GLO_ADMIN_PAGES"]."/footer.php"; ?>
