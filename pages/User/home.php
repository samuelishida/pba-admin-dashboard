<?php require $_ENV["GLO_ADMIN_PAGES"]."/header.php"; ?>
	<!-- create your own reporting method to allow users to choose periods of time -->
	<form name="period" id="period" action="/api/v1/method" method="post">
		<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
		<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
		<input type="hidden" name="start_date" id="report_start_date" value="<?= (new DateTime("now"))->modify('-30 days')->format(DateTime::ISO8601) ?>" />
		<input type="hidden" name="end_date" id="report_end_date" value="<?= (new DateTime("now"))->format(DateTime::ISO8601) ?>" />
	</form>
	<div class="row">
		<div class="col-md-7"></div>
		<div class="col-md-5 text-center">
			<div class="card p-0">
				<div id="reportrange" style="border-radius: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;">
					<i class="far fa-calendar-alt"></i>&nbsp;
					<span></span> <i class="fa fa-caret-down"></i>
				</div>
			</div>
		</div>
	</div>
	<!-- Start Page Content -->
	<div class="row">
		<div class="d-flex align-items-stretch col-md-3">
			<div class="card p-30 width-100">
				<div class="media">
					<div class="media-left meida media-middle">
						<span><i class="fas fa-user-astronaut f-s-40"></i></span>
					</div>
					<div class="media-body media-text-right">
						<h2>123</h2>
						<p class="m-b-0">Example 1</p>
					</div>
				</div>
			</div>
		</div>
		<div class="d-flex align-items-stretch col-md-3">
			<div class="card p-30 width-100">
				<div class="media">
					<div class="media-left meida media-middle">
							<span><i class="fas fa-file-import f-s-40"></i></span>
					</div>
					<div class="media-body media-text-right">
							<h2>43</h2>
							<p class="m-b-0">Example 2</p>
					</div>
				</div>
			</div>
		</div>
		<div class="d-flex align-items-stretch col-md-3">
			<div class="card p-30 width-100">
				<div class="media">
					<div class="media-left meida media-middle">
						<span><i class="fas fa-hand-point-up f-s-40"></i></span>
					</div>
					<div class="media-body media-text-right">
						<h2>4</h2>
						<p class="m-b-0">Example 3</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card p-30">
				<div class="media">
					<div class="media-left media media-middle">
							<span><i class="fas fa-street-view f-s-40"></i></span>
					</div>
					<div class="media-body media-text-right">
							<h2>11</h2>
							<p class="m-b-0">Example 4</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-title">
						<h4><i class="em em-heart"></i> List Example </h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
									<tr>
											<th>#</th>
											<th>Column 1</th>
											<th>Column 2</th>
									</tr>
							</thead>
							<tbody>
								<tr>
									<td><span class="badge badge-info">1</span></td>
									<td>Value 1</td>
									<td><a href="#">Value 2</a></td>
								</tr>
								<tr><td colspan="4"><p class="text-center"> No data do display example.</p></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Content -->
<?php require $_ENV["GLO_ADMIN_PAGES"]."/footer.php"; ?>
<script>
	$(function() {
		moment.locale('pt-br');
		var start = moment("<?= (new DateTime("now"))->modify('-30 days')->format(DateTime::ISO8601) ?>");
		var end = moment("<?= (new DateTime("now"))->format(DateTime::ISO8601) ?>");

		function cb(start, end, label) {
			// console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			$("#report_start_date").val(start.toISOString());
			$("#report_end_date").val(end.toISOString());
		}

		$('#reportrange').daterangepicker({
			ranges: {
				'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
				'Últimos 30 Days': [moment().subtract(29, 'days'), moment()],
				'Este mês': [moment().startOf('month'), moment().endOf('month')],
				'Mês passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			"locale": {
				"format": "DD/MM/YYYY",
				"separator": " - ",
				"applyLabel": "Aplicar",
				"cancelLabel": "Cancelar",
				"fromLabel": "De",
				"toLabel": "Até",
				"customRangeLabel": "Personalizado",
				"weekLabel": "Semana",
				"daysOfWeek": [
					"Dom",
					"Seg",
					"Ter",
					"Qua",
					"Qui",
					"Sex",
					"Sáb"
				],
				"monthNames": [
					"Janeiro",
					"Fevereiro",
					"Março",
					"Abril",
					"Maio",
					"Junho",
					"Julho",
					"Agosto",
					"Setembro",
					"Outubro",
					"Novembro",
					"Dezembro"
				],
				"firstDay": 1
			},
			"startDate": "<?= (new DateTime("now"))->modify('-1 year')->format("d/m/Y") ?>",
			"endDate": "<?= (new DateTime("now"))->format("d/m/Y") ?>",
			"maxDate": moment(),
			"opens": "left"
		}, cb);

		cb(start, end);
	});
	$("#reportrange").on('apply.daterangepicker',function(ev, picker){
		$("#period").submitHandler();
	});
	$("#period").on("treat", function(event, response){
		switch (response.status){
			case 200:
				/* HTTP 200: OK */
				location.reload();
			break;
			default:
				toastr.error('<?= _("system.get_report_date_error") ?>','<?= _("system.report_error") ?>',{
					"positionClass": "toast-top-full-width",
					timeOut: 5000,
					"closeButton": true,
					"debug": false,
					"newestOnTop": true,
					"progressBar": true,
					"preventDuplicates": true,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut",
					"tapToDismiss": false
				});
			break;
		}
	});
</script>
