<?php require $_ENV["GLO_ADMIN_PAGES"]."/header.php"; ?>
	<!-- Start Page Content -->
	<div class="row">
		<div class="col-lg-12 text-right">
			<button value="/users/new" class="btn btn-primary m-b-10 link"><i class="fa fa-plus"></i> <?= _("user.create") ?> </button>
		</div>
	</div>
	<div class="row d-flex align-items-stretch">
	<?php
		foreach ($users as $user){
			?>
				<div class="col-lg-6 d-flex align-items-stretch">
					<div class="card" style="width: 100%;">
						<div class="card-body">
							<div class="card-two">
								<header>
									<div class="avatar">
											<img src="<?= $user->showProfilePicture(); ?>" />
									</div>
								</header>
								<h3><?= $user->getName(); ?></h3>
								<div class="desc">
									<?= $user->getBio(); ?>
								</div>
								<div class="contacts">
									<a href="<?= $user->getFacebook(); ?>"><i class="fab fa-facebook"></i></a>
									<a href="<?= $user->getLinkedin(); ?>"><i class="fab fa-linkedin"></i></a>
									<a href="<?= $user->getTwitter(); ?>"><i class="fab fa-twitter"></i></a>
									<a href="<?= $user->getYoutube(); ?>"><i class="fab fa-youtube"></i></a>
								</div>
								<div class="clear"></div>
								<div class="text-center">
									<button value="/users/view/<?= $user->getId(); ?>" class="btn btn-dark btn-outline m-b-10 link"><i class="fa fa-search"></i></button>
									<button value="/users/edit/<?= $user->getId(); ?>" class="btn btn-dark btn-outline m-b-10 link"><i class="fas fa-pencil-alt"></i></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php
		}
	?>
	</div>
	<div class="row">
			<div class="col-lg-12">
					<div class="card">
							<div class="card-body">
									<h4 class="card-title"><?= _("user.list"); ?></h4>
									<h6 class="card-subtitle"><?= _("user.list_all"); ?></h6>
                  <div class="table-responsive m-t-40">
										<input id="user_table_csrf_name" type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
										<input id="user_table_csrf_key" type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
										<table id="users_table" style="width: 100%;" class="datatable table table-bordered table-striped">
											<thead>
												<tr>
													<th><?= _("user.list.name"); ?></th>
													<th><?= _("user.list.username"); ?></th>
													<th><?= _("user.list.date"); ?></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
							</div>
					</div>
			</div>
<!-- End Page Content -->
<?php require $_ENV["GLO_ADMIN_PAGES"]."/footer.php"; ?>
<script>
	$(document).ready(function(){
		$("#users_table").DataTable({
			"language": {
				/*"paginate": {
					"next": "",
					"previous": ""
				},*/
				"processing": '<i class="fa fa-spinner fa-pulse"></i> Processing...'
			},
			"dom": '<"top"lf>rt<"bottom"pi><"clear">',
			"columnDefs": [
					{ "searchable": false, "orderable": false, "targets": 3 },
					{ "responsivePriority": 1, "targets": 0},
					{ "responsivePriority": 2, "targets": 3},
					{ "responsivePriority": 3, "targets": 1},
					{ "responsivePriority": 4, "targets": 2}
				],
			"lengthChange": true,
			"processing": true,
			"serverSide": true,
			"responsive": true,
			"ajax": {
				"url": "/api/v1/user/datatable",
				"contentType": "application/json",
				"type": "POST",
				"data": function (d) {
					return JSON.stringify($.extend( {}, d, {"csrf_name" : $("#user_table_csrf_name").val(),"csrf_value" : $("#user_table_csrf_key").val()}));
				},
				"dataFilter": function (d){
					var response = JSON.parse(d);
					$(this).updateCSRF(response.csrf);
					return JSON.stringify(response.data);
				},
				"error": function (d){
					$("#message").html('<div class="error"><p><i class="fa fa-times-circle"></i> Something went wrong while retrieving table data. Try again later.</p></div>');
					$(".dataTables_processing").addClass("error-message");
					$(".dataTables_processing").html('<i class="fa fa-times"></i> Error');
				}
			}
		});
	});
</script>
