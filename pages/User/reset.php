<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Tell the browser to be responsive to screen width -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- Favicon icon -->
		<link rel="icon" type="image/png" href="<?=$_ENV["hostname"] ?>/admin/assets/images/favicon.png">
		<title><?= $_ENV["GLO_APP_NAME"]; ?></title>
		<?php require $_ENV["GLO_ADMIN_PAGES"]."/css.php"; ?>
	</head>

	<body class="fix-header fix-sidebar">
		<!-- Preloader - style you can find in spinners.css -->
		<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50">
				<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
		</div>
		<!-- Main wrapper  -->
		<div id="main-wrapper">
			<div class="unix-login">
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-lg-4">
							<div class="login-content card">
								<div class="form-validation">
									<div class="login-form form-valide">
										<div class="row mb-5">
											<span style="margin-left: auto;margin-right: auto;display:block;">
												<img src="/admin/assets/images/logo_icon.svg" alt="homepage" style="width: 5em;" class="dark-logo" />
												<img src="/admin/assets/images/logo_text.svg" alt="homepage" style="width: 10em;" class="dark-logo" />
											</span>
										</div>
											<?php
											if (isset($token) && $token != null){
												if ($valid_token){
													?>
													<form id="reset" name="reset" action="/api/v1/user/reset/password" method="post" class="validate">
														<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
														<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
														<input type="hidden" id="token" name="token" value="<?= $token ?>" />
														<div class="form-group">
															<div>
																<input type="password" id="reset_password" name="password" placeholder="<?= _("user.reset_password.new_password"); ?>" class="form-control" required/>
															</div>
														</div>
														<div class="form-group">
															<div>
																<input type="password" id="reset_confirm_password" name="confirm_password" placeholder="<?= _("user.reset_password.confirm_password"); ?>" class="form-control" required />
															</div>
														</div>
														<input type="submit" value="<?= _("user.reset_password.submit_new_password"); ?>" class="btn btn-primary btn-flat m-b-30 m-t-30"/>
													</form>
													<?php
												}else{
													?>
													<h5><?= _("user.reset_password.invalid_token") ?></h5>
													<p><?= _("user.reset_password.expired_token") ?></p>
													<?php
												}
											}else{
											?>
											<form id="reset" name="reset" action="/api/v1/user/reset/token" method="post" class="validate">
													<input type="hidden" name="<?= $csrf_name_key; ?>" value="<?= $csrf_name; ?>" class="csrf-name" />
													<input type="hidden" name="<?= $csrf_value_key; ?>" value="<?= $csrf_value; ?>" class="csrf-value" />
													<div class="form-group">
														<div>
															<input type="email" id="reset_email" name="email" placeholder="<?= _("user.reset_password.email"); ?>" class="form-control" required />
														</div>
													</div>
													<input type="submit" value="<?= _("user.reset_password.reset_my_password"); ?>" class="btn btn-primary btn-flat m-b-30 m-t-30" />
											</form>
											<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- End Wrapper -->
	<!-- All Jquery -->
	<?php require $_ENV["GLO_ADMIN_PAGES"]."/script.php"; ?>
	<script>
		$(document).ready(function(){
			<?php
				if (isset($token) && $token != null){
					?>
					$("#reset").on("treat", function(event, response){
						switch (response.status){
							case 200:
								/* HTTP 200: OK */
								if (response.data.success == true){
									window.location.href = "/login/success";
								}else{
									toastr.error('<?= _("system.try.again") ?>', '<?= _("user.reset_password.failed") ?>',{
										"positionClass": "toast-top-full-width",
										timeOut: 5000,
										"closeButton": true,
										"debug": false,
										"newestOnTop": true,
										"progressBar": true,
										"preventDuplicates": true,
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut",
										"tapToDismiss": false
									})
								}
							break;
							default:
								toastr.error('<?= _("system.unknown.error") ?>', '<?= _("user.reset_password.failed") ?>',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false
								})
							break;
						}
					});
					$("#reset").validate({
						ignore: [],
						errorClass: "invalid-feedback animated fadeInDown",
						errorElement: "div",
						errorPlacement: function(e, a) {
							jQuery(a).parents(".form-group > div").append(e)
						},
						highlight: function(e) {
							jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
						},
						success: function(e) {
							jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
						},
						submitHandler:
							function(form) {
								$(form).submitHandler();
								return false;
						},
						rules: {
							password: {
								required: true,
								minlength: 6,
								maxlength: 255
							},
							confirm_password: {
								required: true,
								equalTo: "#reset_password"
							}
						},
						messages: {
							password: {
								required: "<?= _("form.field.required"); ?>",
								minlength: "<?= sprintf(_("form.field.least.characters"), 6); ?>",
								maxlength: "<?= sprintf(_("form.field.max"), 255); ?>"
							},
							confirm_password: {
								required: "<?= _("form.field.required"); ?>",
								equalTo: "<?= _("form.reset_password.match")?>"
							}
						}
					});
					<?php
				}else{
					?>
					$("#reset").on("treat", function(event, response){
						switch (response.status){
							case 200:
								/* HTTP 200: OK */
								if (response.data.success == true){
									window.location.href = "/login/reset";
								}else{
									toastr.error('<?= _("system.try.again") ?>','<?= _("user.reset_password.failed") ?>',{
										"positionClass": "toast-top-full-width",
										timeOut: 5000,
										"closeButton": true,
										"debug": false,
										"newestOnTop": true,
										"progressBar": true,
										"preventDuplicates": true,
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut",
										"tapToDismiss": false
									})
								}
							break;
							default:
								toastr.error('<?= _("system.unknown.error") ?>','<?= _("user.reset_password.failed") ?>',{
									"positionClass": "toast-top-full-width",
									timeOut: 5000,
									"closeButton": true,
									"debug": false,
									"newestOnTop": true,
									"progressBar": true,
									"preventDuplicates": true,
									"onclick": null,
									"showDuration": "300",
									"hideDuration": "1000",
									"extendedTimeOut": "1000",
									"showEasing": "swing",
									"hideEasing": "linear",
									"showMethod": "fadeIn",
									"hideMethod": "fadeOut",
									"tapToDismiss": false
								})
							break;
						}
					});
					$("#reset").validate({
						ignore: [],
						errorClass: "invalid-feedback animated fadeInDown",
						errorElement: "div",
						errorPlacement: function(e, a) {
							jQuery(a).parents(".form-group > div").append(e)
						},
						highlight: function(e) {
							jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
						},
						success: function(e) {
							jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
						},
						submitHandler:
							function(form) {
								$(form).submitHandler();
								return false;
						},
						rules: {
							email: {
								required: true
							}
						},
						messages: {
							email: {
								required: "<?= _("form.field.required"); ?>"
							}
						}
					});
					<?php
				}
			?>
		});
	</script>
</body>
</html>
