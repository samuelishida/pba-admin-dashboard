<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Tell the browser to be responsive to screen width -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- Favicon icon -->
		<link rel="icon" type="image/png" href="<?=$_ENV["hostname"] ?>/admin/assets/images/favicon.png">
		<title>GLO | Admin Dashboard</title>
		<?php require $_ENV["GLO_ADMIN_PAGES"]."/css.php"; ?>
	</head>

	<body class="fix-header fix-sidebar">
		<!-- Preloader - style you can find in spinners.css -->
		<div class="preloader">
		<svg class="circular" viewBox="25 25 50 50">
				<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
		</div>

		<!-- Main wrapper	-->
		<div class="error-page" id="wrapper">
				<div class="error-box">
						<div class="error-body text-center">
								<h1>404</h1>
								<h3 class="text-uppercase"><?= _("system.page.inexistent") ?> </h3>
								<p class="text-muted m-t-30 m-b-30"><?= _("system.action.try_again") ?></p>
								<a class="btn btn-info btn-rounded waves-effect waves-light m-b-40" href="/login"><?= _("system.action.back_to_home") ?></a> </div>
						<footer class="footer text-center">
							<div class="row mb-5">
								<span style="margin-left: auto;margin-right: auto;display:block;">
									<img src="/admin/assets/images/logo_icon.svg" alt="homepage" style="width: 5em;" class="dark-logo" />
									<img src="/admin/assets/images/logo_text.svg" alt="homepage" style="width: 10em;" class="dark-logo" />
								</span>
							</div>
						</footer>
				</div>
		</div>

		<!-- End Wrapper -->
		<!-- All Jquery -->
		<?php require $_ENV["GLO_ADMIN_PAGES"]."/script.php"; ?>
	</body>
</html>
