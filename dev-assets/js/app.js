try {
		/* Import libs */
		window.$ = window.jQuery = require('jquery');
		window.NProgress = require('nprogress');
		window.moment = require('moment');
		window.toastr = require('toastr');
		window.DataTable = require('datatables');
		window.Swal = require('sweetalert2');
		window.SimpleMDE = require('simplemde');
		require('bootstrap');
		require('slimscroll');
		require('jquery-slimscroll');
		require('jquery-form');
		require('jquery-validation');
		require('jquery.validate.file');
		require('image-picker');


		require('sticky-kit/dist/sticky-kit.js');
		require('bootstrap-daterangepicker');
		require('chart.js');

		/* Import scripts */
		require('./lib/pt-br.js');
		require('./lib/sidebarmenu.js');
		require('./lib/custom.min.js');
} catch (e) {
		console.log('Failed to load js libs:', e)
}
