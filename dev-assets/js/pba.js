/*
 *
 *  GLO Javascript
 *
 **/
(function($) {
	/* Update CSRF */
	$.fn.updateCSRF = function(csrf){
		$(".csrf-name").each(function(){
			$(this).val(csrf.name);
		});
		$(".csrf-value").each(function(){
			$(this).val(csrf.value);
		});
	}

	$.fn.submitHandler = function(){
		NProgress.start();
		if ($(this).valid()) {
			var form = $(this);
			var data = $(form).serializeJSON();
			$.ajaxSetup({contentType:"application/json;charset=utf-8", dataType: "json"});
			NProgress.set(0.4);
			$.post($(form).attr('action'), data, function(data, textStatus, response){
				form.trigger('treat', [{"status": response.status, "data": data}]);
				NProgress.inc();
			}).fail(function(response){
				form.trigger('treat', [{"status": response.status, "data": data}]);
				NProgress.inc();
			}).always(function(data, textStatus, response){
				if (typeof data.csrf !== 'undefined'){
					$(this).updateCSRF(data.csrf);
				}
				NProgress.done();
			});
		}
	}

	$.fn.submitHandlerUpload = function(){
		NProgress.start();
		if ($(this).valid()) {
			var form = $(this);
			$.ajaxSetup({contentType:"application/json;charset=utf-8", dataType: "json"});
			NProgress.set(0.4);
			$(form).ajaxSubmit({
				dataType: 'json',
				success: function(data, textStatus, response){
					$(form).trigger('treat', [{"status": response.status, "data": data}]);
					if (typeof data.csrf !== 'undefined'){$(this).updateCSRF(data.csrf);}
					NProgress.inc();
				},
				error: function(response){
					$(form).trigger('treat', [{"status": response.status }]);
					if (typeof data.csrf !== 'undefined'){$(this).updateCSRF(data.csrf);}
					NProgress.inc();
				},
				always: function(response){
					NProgress.done();
				}
			});
		}
	}

	/* jQuery Plugin Serialize JSON */
	$.fn.serializeJSON = function() {

		 var o = {};
		 var a = this.serializeArray();
		 $.each(a, function() {
				 if (o[this.name]) {
						 if (!o[this.name].push) {
								 o[this.name] = [o[this.name]];
						 }
						 o[this.name].push(this.value || '');
				 } else {
						 o[this.name] = this.value || '';
				 }
		 });
		 return JSON.stringify(o);
	};
	/* jQuery Exists */
	$.fn.exists = function(){return this.length>0;}

	/* Jquery Validator Methods */
	$.validator.addMethod("greaterThan", function(value, element, params) {
				if (!/Invalid|NaN/.test(moment(value, 'DD/MM/YYYY H:mm').toDate())) {
					return moment(value, 'DD/MM/YYYY H:mm').toDate() > moment($(params).val(), 'DD/MM/YYYY H:mm').toDate();
			}
			return isNaN(value) && isNaN($(params).val()) || (Number(value) > Number($(params).val()));
	},'Must be greater than');

	$.validator.addMethod("smallerThan",
	function(value, element, params) {

			if (!/Invalid|NaN/.test(moment(value, 'DD/MM/YYYY H:mm').toDate())) {
					return moment(value, 'DD/MM/YYYY H:mm').toDate() < moment($(params).val(), 'DD/MM/YYYY H:mm').toDate();
			}

			return isNaN(value) && isNaN($(params).val())
					|| (Number(value) < Number($(params).val()));
	},'Must be smaller than');

	$.validator.addMethod('positive', function(value){ return Number(value) > 0; }, 'Enter a positive number.');
})(jQuery);

/*
 *
 *	jQuery Document Ready
 *
 **/
$(document).ready(function(){
	$("#logout").on("click", function(){
		$("#app_logout").submit();
	});
	$("#app_logout").on("treat", function(event, response){
		switch (response.status){
			case 200:
				/* HTTP 200: OK */
				window.location.href = "/";
			break;
			default:
				/* Something is wrong */
				$("#message").addClass("error");
				$("#message").html("<p>Ops. Our server is facing some temporary issues. Please, try again later.</p>");
			break;
		}
	});

	/* Link click */
	$('body').on("click", '.link', function(){
		window.location.href = $(this).val();
	});

	/* Form upload submit */
	$('body').on('submit', 'form.upload', function(e) {
		e.preventDefault();
		$(this).submitHandlerUpload();
		return false;
	});

	/* Form submit */
	$('form:not(.validate,.upload,.hook)').on('submit', function(e) {
		e.preventDefault();
		$(this).submitHandler();
		return false;
	});

	if ($(".editor").length){
		var simplemde = new SimpleMDE({
			element: $(".editor")[0],
			forceSync: true,
			autosave: {
				enabled: true,
				uniqueId: $($(".editor")[0]).attr("id"),
				delay: 1000
			}
		});
		$($(".editor")[0]).data("simplemde", simplemde);
	}
});

$(function () {
	$('[data-toggle="tooltip"]').tooltip()
})

NProgress.start();
window.onload = function () { NProgress.done(); }
window.onbeforeunload = function () { NProgress.start(); }
window.onpopstate = function () { NProgress.start(); }
